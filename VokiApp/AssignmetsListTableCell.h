//
//  AssignmetsListTableCell.h
//  VokiApp
//
//  Created by brst on 17/01/18.
//  Copyright © 2018 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssignmetsListTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *view1;
@property (strong, nonatomic) IBOutlet UIView *view2;
@property (strong, nonatomic) IBOutlet UIView *view3;

@property (strong, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *filterTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *writingRequiredLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *commentLabel;
@property (strong, nonatomic) IBOutlet UILabel *commentDescLabel;
@property (strong, nonatomic) IBOutlet UIImageView *separatorView;
@property (strong, nonatomic) IBOutlet UIButton *attachmentButtn;

@end
