var Timer;
function RunImagefile(bundlePath,spriteSheet,colorString,accessoryPath,Costume,fhair,Glasses,Hair,Hat,mouth,Necklace,Props)
{
    
    //    alert(bundlePath+accessoryPath+"Costume/"+Costume+"/Front.ohv2.png");
    charObj.unloadPersona();
    charObj.loadPersona(bundlePath+"/assets/"+spriteSheet+"?cs="+colorString,
                        function(url, error) {
                        console.log( url + error);
                        },
                        function(urlg) { },
                        function(url) {
                        if(Costume != 0)
                        charObj.loadAccessory2([["Front", bundlePath+accessoryPath+"Costume/"+Costume+"/Front.ohv2.png"]],
                                               "Costume",  //Acc accType
                                               Costume,     //Acc id
                                               "13",       // Acc compatId
                                               "costume", //Acc name
                                               function(success)
                                               {
                                               
                                               }
                                               );
                        if(fhair != 0)
                        charObj.loadAccessory2([["Left" , bundlePath+accessoryPath+"fhair/"+fhair+"/Left.ohv2.png"],
                                                ["Right" , bundlePath+accessoryPath+"fhair/"+fhair+"/Right.ohv2.png"]],
                                               "fhair",  //Acc accType
                                               fhair,     //Acc id
                                               "13",       // Acc compatId
                                               "fhair", //Acc name
                                               function(success)
                                               {
                                               
                                               }
                                               );
                        if(Glasses != 0)
                        charObj.loadAccessory2([["Mirror" , bundlePath+accessoryPath+"Glasses/"+Glasses+"/Mirror.ohv2.png"]],
                                               "Glasses",  //Acc accType
                                               Glasses,     //Acc id
                                               "13",       // Acc compatId
                                               "Glasses", //Acc name
                                               function(success)
                                               {
                                               
                                               }
                                               );
                        if(Hair != 0)
                        charObj.loadAccessory2([["Left" , bundlePath+accessoryPath+"Hair/"+Hair+"/Left.ohv2.png"],
                                                ["Right" , bundlePath+accessoryPath+"Hair/"+Hair+"/Right.ohv2.png"],
                                                ["Back" , bundlePath+accessoryPath+"Hair/"+Hair+"/Back.ohv2.png"]], //[Frag fragType, Frag file]
                                               "Hair",  //Acc accType
                                               Hair,     //Acc id
                                               "13",       // Acc compatId
                                               "aiko_hair_4", //Acc name
                                               function(success)
                                               {
                                               //alert()
                                               }
                                               );
                        if(Hat != 0)
                        charObj.loadAccessory2([["Left" , bundlePath+accessoryPath+"Hat/"+Hat+"/Left.ohv2.png"],
                                                ["Right" , bundlePath+accessoryPath+"Hat/"+Hat+"/Right.ohv2.png"]],
                                               "Hat",  //Acc accType
                                               Hat,     //Acc id
                                               "13",       // Acc compatId
                                               "Hat", //Acc name
                                               function(success)
                                               {
                                               
                                               }
                                               );
                        if(mouth != 0)
                        charObj.loadAccessory2([["Front", bundlePath+accessoryPath+"mouth/"+mouth+"/Front.ohv2.png"]],
                                               "mouth",  //Acc accType
                                               mouth,     //Acc id
                                               "13",       // Acc compatId
                                               "mouth", //Acc name
                                               function(success)
                                               {
                                               
                                               }
                                               );
                        if(Necklace != 0)
                        charObj.loadAccessory2([["Front", bundlePath+accessoryPath+"Necklace/"+Necklace+"/Front.ohv2.png"]],
                                               "Necklace",  //Acc accType
                                               Necklace,     //Acc id
                                               "13",       // Acc compatId
                                               "Necklace", //Acc name
                                               function(success)
                                               {
                                               
                                               }
                                               );
                        if(Props != 0)
                        charObj.loadAccessory2([["Front" , bundlePath+accessoryPath+"Props/"+Props+"/Front.ohv2.png"],
                                                ["Back" , bundlePath+accessoryPath+"Props/"+Props+"/Back.ohv2.png"]], //[Frag fragType, Frag file]
                                               "Props",  //Acc accType
                                               Props,     //Acc id
                                               "13",       // Acc compatId
                                               "Props", //Acc name
                                               function(success)
                                               {
                                               
                                               }
                                               );
                        }
                        );
}
function RunXMLfile(bundlePath,spriteSheet,colorString,Costume,fhair,Glasses,Hair,Hat,mouth,Necklace,Props)
{
    stop_mp3();
    
    document.getElementById("character").style.display = 'none';
    showLoader();
    charObj.freeze();
    charObj.unloadPersona();
    var hostJS = new com.oddcast.app.vhss_extraction.VHSSHostJS();
    charObj = hostJS.initAPI();  //do not call more than once
    charObj.loadPersona(bundlePath+"/assets/"+spriteSheet+"?cs="+colorString,
                        function(url, error) {
                        
                        console.log( url + error);
                        },
                        function(urlg) { },
                        function(url) {
                        if(Costume)
                        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Costume/"+Costume+"/desc.xml",
                                                     function(success){
                                                     hideLoader();
                                                     // document.getElementById("character").style.display = 'block';
                                                     charObj.resume();
                                                     console.log("success ...");
                                                     addButtonEvents();
                                                     }
                                                     );
                        
                        if(fhair)
                        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/fhair/"+fhair+"/desc.xml",
                                                     function(success){
                                                     hideLoader();
                                                     if(Props==0 && Necklace==0 && mouth==0 && Hat==0 && Hair==0 && Glasses==0)
                                                     {
                                                     document.getElementById("character").style.display = 'block';
                                                     }
                                                     charObj.resume();
                                                     console.log("success ...");
                                                     addButtonEvents();
                                                     }
                                                     );
                        if(Glasses)
                        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Glasses/"+Glasses+"/desc.xml",
                                                     function(success){
                                                     hideLoader();
                                                     if(Props==0 && Necklace==0 && mouth==0 && Hat==0 && Hair==0)
                                                     {
                                                     document.getElementById("character").style.display = 'block';
                                                     }
                                                     charObj.resume();
                                                     console.log("success ...");
                                                     addButtonEvents();
                                                     }
                                                     );
                        if(Hair)
                        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Hair/"+Hair+"/desc.xml",
                                                     function(success){
                                                     hideLoader();
                                                     if(Props==0 && Necklace==0 && mouth==0 && Hat==0)
                                                     {
                                                     document.getElementById("character").style.display = 'block';
                                                     }
                                                     charObj.resume();
                                                     console.log("success ...");
                                                     addButtonEvents();
                                                     }
                                                     );
                        if(Hat)
                        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Hat/"+Hat+"/desc.xml",
                                                     function(success){
                                                     hideLoader();
                                                     if(Props==0 && Necklace==0 && mouth==0)
                                                     {
                                                     document.getElementById("character").style.display = 'block';
                                                     }
                                                     charObj.resume();
                                                     console.log("success ...");
                                                     addButtonEvents();
                                                     }
                                                     );
                        if(mouth)
                        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/mouth/"+mouth+"/desc.xml",
                                                     function(success){
                                                     hideLoader();
                                                     if(Props==0 && Necklace==0)
                                                     {
                                                     document.getElementById("character").style.display = 'block';
                                                     }
                                                     charObj.resume();
                                                     console.log("success ...");
                                                     addButtonEvents();
                                                     }
                                                     );
                        if(Necklace)
                        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Necklace/"+Necklace+"/desc.xml",
                                                     function(success){
                                                     hideLoader();
                                                     if(Props==0)
                                                     {
                                                     document.getElementById("character").style.display = 'block';
                                                     }
                                                     charObj.resume();
                                                     console.log("success ...");
                                                     addButtonEvents();
                                                     }
                                                     );
                        if(Props)
                        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Props/"+Props+"/desc.xml",
                                                     function(success){
                                                     hideLoader();
                                                     document.getElementById("character").style.display = 'block';
                                                     charObj.resume();
                                                     console.log("success ...");
                                                     addButtonEvents();
                                                     }
                                                     );
                        
                        
                        }
                        );
    
    
}
function RunXMLfileWithColor(bundlePath,spriteSheet,colorString,Costume,fhair,Glasses,Hair,Hat,mouth,Necklace,Props,eyesC,EyeHex,HandC,HandHex,HairC,HairHex,MouthC,MouthHex)
{
    stop_mp3();
    document.getElementById("character").style.display = 'none';
    showLoader();
    charObj.freeze();
    charObj.unloadPersona();
    var hostJS = new com.oddcast.app.vhss_extraction.VHSSHostJS();
    charObj = hostJS.initAPI();  //do not call more than once
    
    charObj.loadPersona(bundlePath+"/assets/"+spriteSheet+"?cs="+colorString,
                        function(url, error) {
                        
                        console.log( url + error);
                        },
                        function(urlg) { },
                        function(url) {
                        if(Costume)
                        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Costume/"+Costume+"/desc.xml",
                                                     function(success){
                                                     hideLoader();
                                                     // document.getElementById("character").style.display = 'block';
                                                     charObj.resume();
                                                     console.log("success ...");
                                                     console.log("IST success is what");
                                                     addButtonEvents();
                                                     }
                                                     );
                        
                        if(fhair)
                        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/fhair/"+fhair+"/desc.xml",
                                                     function(success){
                                                     hideLoader();
                                                     
                                                     if(Props==0 && Necklace==0 && mouth==0 && Hat==0 && Hair==0 && Glasses==0)
                                                     {
                                                     document.getElementById("character").style.display = 'block';
                                                     }
                                                     charObj.resume();
                                                     console.log("success ...");
                                                     console.log("sec success is what");
                                                     addButtonEvents();
                                                     }
                                                     );
                        if(Glasses)
                        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Glasses/"+Glasses+"/desc.xml",
                                                     function(success){
                                                     hideLoader();
                                                     if(Props==0 && Necklace==0 && mouth==0 && Hat==0 && Hair==0)
                                                     {
                                                     document.getElementById("character").style.display = 'block';
                                                     }
                                                     charObj.resume();
                                                     console.log("success ...");
                                                     console.log("Third success is what");
                                                     addButtonEvents();
                                                     }
                                                     );
                        if(mouth)
                        
                        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/mouth/"+mouth+"/desc.xml",
                                                     function(success){
                                                     hideLoader();
                                                     if(Props==0 && Necklace==0 && mouth==0 && Hat==0)
                                                     {
                                                     document.getElementById("character").style.display = 'block';
                                                     }
                                                     charObj.resume();
                                                     console.log("success ...");
                                                     
                                                     addButtonEvents();
                                                     },
                                                     function(error)
                                                     {
                                                     }
                                                     );
                        
                        if(Hat)
                        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Hat/"+Hat+"/desc.xml",
                                                     function(success){
                                                     hideLoader();
                                                     if(Props==0 && Necklace==0 && mouth==0)
                                                     {
                                                     
                                                     document.getElementById("character").style.display = 'block';
                                                     }
                                                     charObj.resume();
                                                     console.log("success ...");
                                                     console.log("Fifth success is what");
                                                     
                                                     addButtonEvents();
                                                     }
                                                     );
                        
                        
                        if(Hair)
                        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Hair/"+Hair+"/desc.xml",
                                                     function(success){
                                                     hideLoader();
                                                     
                                                     
                                                     if(Props==0 && Necklace==0 )
                                                     {
                                                     document.getElementById("character").style.display = 'block';
                                                     }
                                                     
                                                     charObj.resume();
                                                     console.log("success ...");
                                                     console.log("Fourth success is what");
                                                     addButtonEvents();
                                                     }
                                                     );
                        
                        if(Necklace)
                        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Necklace/"+Necklace+"/desc.xml",
                                                     function(success){
                                                     hideLoader();
                                                     
                                                     if(Props==0)
                                                     {
                                                     document.getElementById("character").style.display = 'block';
                                                     }
                                                     charObj.resume();
                                                     console.log("success ...");
                                                     addButtonEvents();
                                                     }
                                                     );
                        if(Props)
                        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Props/"+Props+"/desc.xml",
                                                     function(success){
                                                     
                                                     hideLoader();
                                                     document.getElementById("character").style.display = 'block';
                                                     charObj.resume();
                                                     console.log("success ...");
                                                     addButtonEvents();
                                                     }
                                                     );
                        
                        charObj.setColor(eyesC, EyeHex);
                        charObj.setColor(HandC, HandHex);
                        charObj.setColor(HairC, HairHex);
                        charObj.setColor(MouthC, MouthHex);
                        }
                        );
}
function resumeChar()
{
    charObj.resume();
    clearTimeout(Timer);
}
function fetchAPILink()
{
    var charOHString = charObj.getOHUrl(null);
    return charOHString;
}
function LoadAllAccessory(Costume,fhair,Glasses,Hair,Hat,mouth,Necklace,Props)
{
    if(Costume)
        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Costume/"+Costume+"/desc.xml",
                                     function(success){
                                     console.log("success ...");
                                     }
                                     );
    if(fhair)
        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/fhair/"+fhair+"/desc.xml",
                                     function(success){
                                     console.log("success ...");
                                     }
                                     );
    if(Glasses)
        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Glasses/"+Glasses+"/desc.xml",
                                     function(success){
                                     console.log("success ...");
                                     }
                                     );
    if(Hair)
        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Hair/"+Hair+"/desc.xml",
                                     function(success){
                                     console.log("success ...");
                                     }
                                     );
    if(Hat)
        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Hat/"+Hat+"/desc.xml",
                                     function(success){
                                     console.log("success ...");
                                     }
                                     );
    if(mouth)
        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/mouth/"+mouth+"/desc.xml",
                                     function(success){
                                     console.log("success ...");
                                     }
                                     );
    if(Necklace)
        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Necklace/"+Necklace+"/desc.xml",
                                     function(success){
                                     console.log("success ...");
                                     }
                                     );
    if(Props)
        charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Props/"+Props+"/desc.xml",
                                     function(success){
                                     console.log("success ...");
                                     }
                                     );
}
function LoadCostumeAccessory(bundlePath,spriteSheet,colorString,Costume)
{
    //    alert(Costume);
    //        charObj.loadPersona(bundlePath+"/assets/"+spriteSheet+"?cs="+colorString,
    //                            function(url, error) {
    //                            console.log( url + error);
    //                            },
    //                            function(urlg) { },
    //                            function(url) {
    charObj.loadAccessoryFromXML(bundlePath+"/assets/", "img/acc/id/Costume/"+Costume+"/desc.xml",
                                 function(success){
                                 }
                                 );
    //                            }
    //                            );
}
function LoadfhairAccessory(bundlePath,spriteSheet,colorString,fhair)
{
    //        charObj.loadPersona(bundlePath+"/assets/"+spriteSheet+"?cs="+colorString,
    //                            function(url, error) {
    //                            console.log( url + error);
    //                            },
    //                            function(urlg) { },
    //                            function(url) {
    charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/fhair/"+fhair+"/desc.xml",
                                 function(success){
                                 console.log("success ...");
                                 }
                                 );
    //                            }
    //                            );
}
function LoadGlassesAccessory(bundlePath,spriteSheet,colorString,Glasses)
{
    //        charObj.loadPersona(bundlePath+"/assets/"+spriteSheet+"?cs="+colorString,
    //                            function(url, error) {
    //                            console.log( url + error);
    //                            },
    //                            function(urlg) { },
    //                            function(url) {
    charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Glasses/"+Glasses+"/desc.xml",
                                 function(success){
                                 console.log("success ...");
                                 }
                                 );
    //                            }
    //                            );
}
function LoadHairAccessory(bundlePath,spriteSheet,colorString,Hair)
{
    // alert(Hair);
    //        charObj.loadPersona(bundlePath+"/assets/"+spriteSheet+"?cs="+colorString,
    //                            function(url, error) {
    //                            console.log( url + error);
    //                            },
    //                            function(urlg) { },
    //                            function(url) {
    charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Hair/"+Hair+"/desc.xml",
                                 function(success){
                                 console.log("success ...");
                                 }
                                 );
    //                            }
    //                            );
}
function LoadHatAccessory(bundlePath,spriteSheet,colorString,Hat)
{
    //        charObj.loadPersona(bundlePath+"/assets/"+spriteSheet+"?cs="+colorString,
    //                            function(url, error) {
    //                            console.log( url + error);
    //                            },
    //                            function(urlg) { },
    //                            function(url) {
    charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Hat/"+Hat+"/desc.xml",
                                 function(success){
                                 console.log("success ...");
                                 }
                                 );
    //                            }
    //                            );
}
function LoadmouthAccessory(bundlePath,spriteSheet,colorString,mouth)
{
    //        charObj.loadPersona(bundlePath+"/assets/"+spriteSheet+"?cs="+colorString,
    //                            function(url, error) {
    //                            console.log( url + error);
    //                            },
    //                            function(urlg) { },
    //                            function(url) {
    charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/mouth/"+mouth+"/desc.xml",
                                 function(success){
                                 console.log("success ...");
                                 }
                                 );
    //                            }
    //    );
}
function LoadNecklaceAccessory(bundlePath,spriteSheet,colorString,Necklace)
{
    // alert("testing");
    //        charObj.loadPersona(bundlePath+"/assets/"+spriteSheet+"?cs="+colorString,
    //                            function(url, error) {
    //                            console.log( url + error);
    //                            },
    //                            function(urlg) { },
    //                            function(url) {
    charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Necklace/"+Necklace+"/desc.xml",
                                 function(success){
                                 console.log("success ...");
                                 }
                                 );
    //                            }
    //                            );
}
function LoadPropsAccessory(bundlePath,spriteSheet,colorString,Props)
{
    
    //        charObj.loadPersona(bundlePath+"/assets/"+spriteSheet+"?cs="+colorString,
    //                            function(url, error) {
    //                            console.log( url + error);
    //                            },
    //                            function(urlg) { },
    //                            function(url) {
    charObj.loadAccessoryFromXML(bundlePath+"/assets/",	 "img/acc/id/Props/"+Props+"/desc.xml",
                                 function(success)
                                 {
                                 console.log("success ...");
                                 
                                 }
                                 );
    //                            }
    //                            );
}
function changeEyeColorToGreen(partBody,colorHex)
{
    
    charObj.setColor(partBody, colorHex);
}

function getBodyPartColor(partBody)
{
    var color1 = charObj.getColor(partBody);
    return color1
}

function iscolorBool(partBody)
{
    
    var iscolor = charObj.isColorable(partBody);
    return iscolor
}

function isAlpha(part)
{
    var isalpha = charObj.isAlphable(part);
    return isalpha
    
}

function getAlpha(part)
{
    var getAlpha = charObj.getAlpha(part);
    return getAlpha
    
}
function changeEyeColorToRed()
{
    charObj.setColor("eyes", 255); //set the eyes blue
}
function changeFront_1(path)
{
    //    alert(path)
    charObj.loadAccessory2([["Front", path+"/assets/img/mam/2415/Front.ohv2.png"]], //[Frag fragType, Frag file]
                           "Costume",  //Acc accType
                           "2415",     //Acc id
                           "13",       // Acc compatId
                           "anime_green_costume", //Acc name
                           function(success)
                           {
                           console.log("success ...");
                           }
                           );
}
function changeHairsStyle_1(path)
{
    charObj.loadAccessory2([["Left" , path+"/assets/img/mam/3680/Left.ohv2.png"],
                            ["Right" , path+"/assets/img/mam/3680/Right.ohv2.png"],
                            ["Back" , path+"/assets/img/mam/3680/Back.ohv2.png"]], //[Frag fragType, Frag file]
                           "Hair",  //Acc accType
                           "3680",     //Acc id
                           "0",       // Acc compatId
                           "aiko_hair_4", //Acc name
                           function(success){
                           console.log("success .. ");
                           }
                           );
}
function changeCostume(path,AccID)
{
    charObj.loadAccessory2([["Front", path+"/Front.ohv2.png"]], //[Frag fragType, Frag file]
                           "Costume",  //Acc accType
                           AccID,     //Acc id
                           "13",       // Acc compatId
                           "anime_green_costume", //Acc name
                           function(success)
                           {
                           console.log("success ...");
                           }
                           );
}
function changeHairsStyle(path,AccID)
{
    charObj.loadAccessory2([["Left" , path+"/Left.ohv2.png"],
                            ["Right" , path+"/Right.ohv2.png"],
                            ["Back" , path+"/Back.ohv2.png"]], //[Frag fragType, Frag file]
                           "Hair",  //Acc accType
                           AccID,     //Acc id
                           "0",       // Acc compatId
                           "aiko_hair_4", //Acc name
                           function(success){
                           console.log("success .. ");
                           }
                           );
}
function LoadXmlFile()
{
    charObj.loadAccessoryFromXML("../assets/", "img/acc/id/Hair/2408/desc.xml",
                                 function(success){
                                 console.log("success .. ");
                                 }
                                 );
}
function play_mp3_1()
{
    charObj.sayUrl("https://vhss.oddcast.com/admin/getAudioByNameMP3.php?audioname=kara_4&acc=237929&play=1", 0.0, true
                   ,function(url, error){// 				: FileErrorCallback
                   console.log("audio error:"+error);
                   }
                   ,function(url){//							: AudioLoadedCallback
                   console.log("audio loaded:"+url);
                   }
                   ,function(url, time){//							: AudioStartedCallback
                   console.log("audio started:"+time);
                   }
                   ,function(url, time){//							: AudioFinishedCallback:
                   console.log("audio finished:"+time);
                   }
                   );
}

function play_mp3_2(path)
{
    showLoader();
    charObj.sayUrl(path, 0.0, true
                   ,function(url, error)
                   {// 				: FileErrorCallback
                   console.log("audio error:"+error);
                   myJavascriptFn();
                   hideLoader();
                   // alert(error)
                   }
                   ,function(url){//							: AudioLoadedCallback
                   console.log("audio loaded:"+url);
                   
                   // hideLoader();
                   }
                   ,function(url, time){//							: AudioStartedCallback
                   console.log("audio started:"+time);
                   myJavascriptFn();
                   hideLoader();
                   }
                   ,function(url, time){//							: AudioFinishedCallback:
                   console.log("audio finished:"+time);
                   //hideLoader();
                   testFunction();

                   console.log("hello shikha");
                   myJavascriptFn();
                   
                   }
                   );
}

function myJavascriptFn() {
    
    window.location = "js-call:myObjectiveCFunction";
    
    
}
function stop_mp3(delay)
{
    if(delay == 1)
    {
        setTimeout(function(){
                   charObj.stopPlayingAudio();
                   }, 1000);
    }
    else
    {
        charObj.stopPlayingAudio();
    }
    
}

function Createimage()
{
    
    var canvas = document.getElementById('hostCanvas');
    
    return canvas.toDataURL() ;
}


function stop_mp3()
{
    
    charObj.stopPlayingAudio();
}
function setGaze(degrees, duration, magnitude)
{
    charObj.setGaze(degrees, duration, magnitude);
}

function showLoader()
{
    document.getElementById("__loading__").style.display = 'block';
}
function hideLoader()
{
    document.getElementById("__loading__").style.display = 'none';
}

function defaultupdateBackground(imagePath,x,y)
{
    document.body.style.backgroundImage = "url('"+imagePath+"')";
    document.body.style.backgroundSize = x+" "+y;
    document.body.style.backgroundRepeat = "no-repeat";
    document.body.style.backgroundPosition = "0px 0px";
}

function defaultloadBackgroundImage(url,x,y)
{
    var img = document.getElementById("dummy_image");
    img.onload = function()
    {
        defaultupdateBackground(url,x,y)
    };
    img.src = url;
}

function updateBackground(imagePath,w,h)
{
    var dw = window.innerWidth;
    var dh = window.innerHeight;
    w = parseInt(w);
    h = parseInt(h);
    
    var hratioo = w/h;
    var hratio = Math.round(hratioo * 10) / 10;
    if(w < dw && h < dh)
    {
        var wrequiredd = dw-w;
        var hrequiredd = h+(wrequiredd/hratio);
        var hrequired = Math.round(hrequiredd);
        var wrequired = w+wrequiredd;
        xposition = 0;
        yposition = (hrequired-dh)/2;
    }
    else if(w > dw && h > dh)
    {
        var wrequiredd = w-dw;
        var hrequiredd = h-(wrequiredd/hratio);
        var hrequired = Math.round(hrequiredd);
        var wrequired = w-wrequiredd;
        xposition = 0;
        yposition = (hrequired-dh)/2;
    }
    else if(w < dw && h > dh)
    {
        var wrequiredd = w-dw;
        var hrequiredd = h-(wrequiredd/hratio);
        var hrequired = Math.round(hrequiredd);
        var wrequired = w-wrequiredd;
        xposition = 0;
        yposition = (hrequired-dh)/2;
    }
    else if(w > dw && h < dh)
    {
        var wrequiredd = w-dw;
        var hrequiredd = h-(wrequiredd/hratio);
        var hrequired = Math.round(hrequiredd);
        var wrequired = w-wrequiredd;
        xposition = 0;
        yposition = (hrequired-dh)/2;
    }
    else
    {
        var hrequired = h;
        var wrequired = w;
        xposition = 0;
        yposition = 0;
    }
    if(hrequired < dh)
    {
        var nhrequiredd = dh-hrequired;
        var wrequired = wrequired+(nhrequiredd*hratio);
        var wrequired = Math.round(wrequired);
        var hrequired = hrequired+nhrequiredd;
        xposition = (wrequired-dw)/2;
        yposition = 0;
    }
    if(xposition > 0){ xposition = '-'+xposition; }
    if(yposition > 0){ yposition = '-'+yposition; }
    document.body.style.backgroundImage = "url('"+imagePath+"')";
    document.body.style.backgroundPosition = xposition+"px "+yposition+"px";
    document.body.style.backgroundSize = wrequired+"px "+hrequired+"px";
    document.body.style.backgroundRepeat = "no-repeat";
    
    
}

function loadBackgroundImage(url,x,y)
{
    var img = document.getElementById("dummy_image");
    img.onload = function() {
        updateBackground(url,x,y)
    };
    img.src = url;
}

function charcMove(x)
{
    charObj.setHostPos(x,0)
}

function addButtonEvents()
{
    
    var elem = document.getElementById("_play");
    if(typeof elem !== 'undefined' && elem !== null)
    {
        document.getElementById("_play").removeEventListener("click", _clickPlay);
        document.getElementById("_play").addEventListener("click", function(){
                                                          
                                                          _clickPlay();
                                                          })
    }
    var elem = document.getElementById("_pause");
    if(typeof elem !== 'undefined' && elem !== null)
    {
        document.getElementById("_play").removeEventListener("click", _clickPause);
        document.getElementById("_pause").addEventListener("click", function(){
                                                           _clickPause();
                                                           })
    }
}
function _clickPause()
{
    charObj.stopPlayingAudio();
}
function _clickPlay(path)
{
    showLoader();
    charObj.sayUrl(path, 0.0, true
                   ,function(url, error)
                   {//     : FileErrorCallback
                   console.log("audio error:"+error);
                   }
                   ,function(url){//       : AudioLoadedCallback
                   console.log("audio loaded:"+url);
                   document.getElementById("_play").style.display = 'none';
                   document.getElementById("_pause").style.display = 'inline-block';
                   }
                   ,function(url, time){//       : AudioStartedCallback
                   hideLoader();
                   console.log("audio started:"+time);
                   }
                   ,function(url, time){//       : AudioFinishedCallback:
                   console.log("audio finished:"+time);
                   document.getElementById("_pause").style.display = 'none';
                   document.getElementById("_play").style.display = 'inline-block';
                   }
                   );
}


function updateBoxPosition(x,y)
{
    
    // document.getElementById("hostDiv").style.marginLeft = val+"px";
    //charObj.setHostPos(x,y);
        $("#character").animate({left: x+'px'},300);
    //$("#character").animate({left: '200px'},1500);
}

function enableScreen()
{
    charObj.enableOnscreenDebugging(0);
}

function getAcess(typeString)
{
    var getAcessa = charObj.getAccessoryID(typeString);
    return getAcessa
    
}

function positionChar(xpos,ypos,scale)
{
  
    charObj.setHostPos(xpos,ypos);
    charObj.setHostScale(scale);
    
}

function slidingChar(x)
{
    charObj.setX(x);
}
function gethostX()
{
    return charObj.getHostPosX();
}
function getDimension()
{
    return charObj.getHostDimensions();
}
function getYPos()
{
    return charObj.getHostPosY();
}
function updateLoaderPosition(fromLeft,fromTop)
{
    document.getElementById("loaderImg").style.marginLeft = fromLeft+"px";
    document.getElementById("loaderImg").style.marginTop = fromTop+"px";
}
function testFunction()
{
    
    return charObj.getCurrentAudioProgress();
}
function CreateThumbnail(width,height,x,y)
{
    width = parseInt(width);
    height = parseInt(height);
    x = parseInt(x);
    y = parseInt(y);
    var canvas = document.getElementById('hostCanvas');
    // canvas context
    var context = canvas.getContext('2d');
    // get the current ImageData for the canvas
    var data = context.getImageData(0, 0, canvas.width, canvas.height);
    // store the current globalCompositeOperation
    var compositeOperation = context.globalCompositeOperation;
    // create temporary...
    var tempCanvas = document.createElement("canvas"),
    tCtx = tempCanvas.getContext("2d");
    tempCanvas.setAttribute("id", "tempCanv");
    tempCanvas.width = width;
    tempCanvas.height = height;
    
    tCtx.drawImage(canvas,x,y);
    
    var img = tempCanvas.toDataURL("image/png");
    return img;
}
