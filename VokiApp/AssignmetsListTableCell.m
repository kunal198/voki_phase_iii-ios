//
//  AssignmetsListTableCell.m
//  VokiApp
//
//  Created by brst on 17/01/18.
//  Copyright © 2018 brst. All rights reserved.
//

#import "AssignmetsListTableCell.h"

@implementation AssignmetsListTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _subTitleLabel.minimumFontSize = 10.0;
    _subTitleLabel.adjustsFontSizeToFitWidth = YES;
    _subTitleLabel.numberOfLines = 1;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
