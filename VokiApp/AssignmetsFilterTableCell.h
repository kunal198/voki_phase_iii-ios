//
//  AssignmetsFilterTableCell.h
//  VokiApp
//
//  Created by brst on 17/01/18.
//  Copyright © 2018 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssignmetsFilterTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleMainLabel;
@property (strong, nonatomic) IBOutlet UIImageView *titleMainImage;

@end
