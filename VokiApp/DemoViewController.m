//
//  DemoViewController.m
//  VokiApp
//
//  Created by brst on 17/01/18.
//  Copyright © 2018 brst. All rights reserved.
//

#import "DemoViewController.h"
#import "demoHeaderTableCell1.h"
#import "DemoTableCell1.h"

@interface DemoViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    int section1;
    int section2;
    int section3;
    NSIndexPath *selectedRow;
}

@end

@implementation DemoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    CGFloat borderWidth = 3.0f;
    UIColor *borderColor = [UIColor colorWithRed:31/255.0 green:47/255.0 blue:104/255.0 alpha:1.0];
    
    section1 = 0;
    section2 = 0;
    section3 = 0;
    
     _view2.layer.borderColor = borderColor.CGColor;
     _view2.layer.borderWidth = borderWidth;
     _view2.layer.cornerRadius=10;
     _view2.clipsToBounds=YES;
     _view1.layer.cornerRadius=15;
     _view1.clipsToBounds=YES;
     
     _button1.layer.borderColor = borderColor.CGColor;
     _button1.layer.borderWidth = 2.2;
     
     _continue1.layer.cornerRadius=8;
     _continue1.clipsToBounds=YES;
     
     _label1.minimumFontSize = 10.0;
     _label1.adjustsFontSizeToFitWidth = YES;
     _label1.numberOfLines = 3;
    _label1.backgroundColor = [UIColor greenColor];
    
    _view3.hidden = true;
    
    /*_view4.layer.borderColor = borderColor.CGColor;
     _view4.layer.borderWidth = borderWidth;
     _view4.layer.cornerRadius=10;
     _view4.clipsToBounds=YES;
     _view3.layer.cornerRadius=15;
     _view3.clipsToBounds=YES;
     
     _back2.layer.cornerRadius=8;
     _back2.clipsToBounds=YES;
     _submit1.layer.cornerRadius=8;
     _submit1.clipsToBounds=YES;
     
     _cinfirmsubmition1.minimumFontSize = 10.0;
     _cinfirmsubmition1.adjustsFontSizeToFitWidth = YES;
     _cinfirmsubmition1.numberOfLines = 1;
     _areyou1.minimumFontSize = 10.0;
     _areyou1.adjustsFontSizeToFitWidth = YES;
     _areyou1.numberOfLines = 4;
    */
    
    _view5.hidden = true;
    
    /*_view6.layer.borderColor = borderColor.CGColor;
     _view6.layer.borderWidth = borderWidth;
     _view6.layer.cornerRadius=10;
     _view6.clipsToBounds=YES;
     _view5.layer.cornerRadius=15;
     _view5.clipsToBounds=YES;
     
     _yourassignment1.minimumFontSize = 10.0;
     _yourassignment1.adjustsFontSizeToFitWidth = YES;
     _yourassignment1.numberOfLines = 2;*/
    
    
    _view7.hidden = true;
    
    /*_view8.layer.borderColor = borderColor.CGColor;
    _view8.layer.borderWidth = borderWidth;
    _view8.layer.cornerRadius=10;
    _view8.clipsToBounds=YES;
    _view7.layer.cornerRadius=15;
    _view7.clipsToBounds=YES;
    
    _info1.layer.cornerRadius = _info1.frame.size.height / 2.0;
    _info1.clipsToBounds=YES;
    _continue2.layer.cornerRadius=10;
    _continue2.clipsToBounds=YES;
    
    _info1.backgroundColor = [UIColor colorWithRed:108/255.0 green:108/255.0 blue:108/255.0 alpha:1.0];
    _continue2.backgroundColor = [UIColor colorWithRed:108/255.0 green:108/255.0 blue:108/255.0 alpha:1.0];
    */
    _view9.hidden = true;
    _view11.hidden = true;
    
    /*
    _view10.layer.borderColor = borderColor.CGColor;
    _view10.layer.borderWidth = borderWidth;
    _view10.layer.cornerRadius=10;
    _view10.clipsToBounds=YES;
    _view9.layer.cornerRadius=15;
    _view9.clipsToBounds=YES;
    _properuselabel.minimumFontSize = 10.0;
    _properuselabel.adjustsFontSizeToFitWidth = YES;
    _properuselabel.numberOfLines = 1;
    
    _view12.layer.borderColor = borderColor.CGColor;
    _view12.layer.borderWidth = borderWidth;
    _view12.layer.cornerRadius=10;
    _view12.clipsToBounds=YES;
    _view11.layer.cornerRadius=15;
    _view11.clipsToBounds=YES;
    
    _properuselabel2.minimumFontSize = 10.0;
    _properuselabel2.adjustsFontSizeToFitWidth = YES;
    _properuselabel2.numberOfLines = 1;
     
    _info2.layer.cornerRadius=_info2.frame.size.height/2.0;
    _info2.clipsToBounds=YES;
    
    _thisassignmentlabel.minimumFontSize = 10.0;
    _thisassignmentlabel.adjustsFontSizeToFitWidth = YES;
    _thisassignmentlabel.numberOfLines = 1;
    
    _textview1.layer.borderColor = borderColor.CGColor;
    _textview1.layer.borderWidth = 2.0;
    _textview1.layer.cornerRadius=2;
    _textview1.clipsToBounds=YES;
    
    _back2.layer.cornerRadius=8;
    _back2.clipsToBounds=YES;
    _continue3.layer.cornerRadius=8;
    _continue3.clipsToBounds=YES;
     */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cross1:(id)sender {
    
}
- (IBAction)donotshow1:(id)sender {
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
    {
        return section1;
    }
    else if(section == 1)
    {
        return section2;
    }
    return section3;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell1";
    DemoTableCell1 *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if(indexPath == selectedRow)
    {
        cell.background_View.backgroundColor = [UIColor orangeColor];
        
        cell.title_label.textColor = [UIColor whiteColor];
        cell.label2.textColor = [UIColor whiteColor];
        //cell.wri
    }
    else
    {
        cell.background_View.backgroundColor = [UIColor clearColor];
        
        cell.title_label.textColor = [UIColor colorWithRed:31/255.0 green:47/255.0 blue:104/255.0 alpha:1.0];
        cell.label2.textColor = [UIColor colorWithRed:31/255.0 green:47/255.0 blue:104/255.0 alpha:1.0];
    }
    
    cell.backgroundColor = [UIColor colorWithRed:220.0/255.0 green:225.0/255.0 blue:233.0/255.0 alpha:1.0];
    return cell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *CellIdentifier = @"cell2";
    demoHeaderTableCell1 *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.dropdownButton.tag = section;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    selectedRow = indexPath;
    [tableView reloadData];
    
    _info1.backgroundColor = [UIColor colorWithRed:31/255.0 green:47/255.0 blue:104/255.0 alpha:1.0];
    _continue2.backgroundColor = [UIColor colorWithRed:31/255.0 green:47/255.0 blue:104/255.0 alpha:1.0];
}
- (IBAction)DropDownAction:(id)sender {
    NSLog(@"%lid",(long)[sender tag]);
    
    if([sender tag] == 0)
    {
        if(section1 == 0)
        {
            section1 = 3;
        }
        else
        {
            section1 = 0;
        }
    }
    if([sender tag] == 1)
    {
        if(section2 == 0)
        {
            section2 = 3;
        }
        else
        {
            section2 = 0;
        }
    }
    if([sender tag] == 2)
    {
        if(section3 == 0)
        {
            section3 = 3;
        }
        else
        {
            section3 = 0;
        }
    }
    
    //[_tableView1 reloadSections:[NSIndexSet indexSetWithIndex:[sender tag]] withRowAnimation:UITableViewRowAnimationTop];
    [_tableView1 reloadData];
}

@end
