//
//  AssignmetsFilterTableCell.m
//  VokiApp
//
//  Created by brst on 17/01/18.
//  Copyright © 2018 brst. All rights reserved.
//

#import "AssignmetsFilterTableCell.h"

@implementation AssignmetsFilterTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _titleMainLabel.minimumFontSize = 10.0;
    _titleMainLabel.adjustsFontSizeToFitWidth = YES;
    _titleMainLabel.numberOfLines = 2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
