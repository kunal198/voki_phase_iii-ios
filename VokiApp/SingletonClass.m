//
//  SingletonClass.m
//  VokiApp
//
//  Created by brst on 01/01/18.
//  Copyright © 2018 brst. All rights reserved.
//

#import "SingletonClass.h"
#import "AppDelegate.h"
#import "AssignmetsListTableCell.h"

@implementation SingletonClass

static SingletonClass *sharedAwardCenter = nil;

+ (SingletonClass *)sharedCenter {
    if (sharedAwardCenter == nil) {
        sharedAwardCenter = [[super allocWithZone:NULL] init];
    }
    return sharedAwardCenter;
}

- (id)init {
    if ( (self = [super init]) ) {
        // your custom initialization
    }
    return self;
}

-(BOOL)isInstagramAppInstalled {
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://camera"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        return true;
    } else {
        return false;
    }
}
-(BOOL)isWhatsappInstalled {
    NSURL *whatsappURL = [NSURL URLWithString:@"whatsapp://send"];
    if ([[UIApplication sharedApplication] canOpenURL:whatsappURL]) {
        return true;
    } else {
        return false;
    }
}
-(void)setWhyLoginAttributedDesc:(UILabel*)targetLabel
{
    UIColor* COLOR_BLUE = [UIColor colorWithRed:31/255.0 green:47/255.0 blue:104/255.0 alpha:1.0];
    
    NSTextAttachment *imageAttachment = [[NSTextAttachment alloc] init];
    imageAttachment.image = [UIImage imageNamed:@"dollar"];
    CGFloat imageOffsetY = -3.0;
    imageAttachment.bounds = CGRectMake(0, imageOffsetY, 18, 18);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:imageAttachment];
    NSMutableAttributedString *completeText= [[NSMutableAttributedString alloc] initWithString:@""];
    
    NSMutableAttributedString *textBeforeIcon1 = [[NSMutableAttributedString alloc] initWithString:@"Voki.com"];
    UIFont *font1 = [UIFont fontWithName:@"Futura-Bold" size:18];
    [textBeforeIcon1 addAttribute:NSFontAttributeName value:font1 range:NSMakeRange(0, [textBeforeIcon1 length])];
    [completeText appendAttributedString:textBeforeIcon1];
    
    NSMutableAttributedString *textBeforeIcon2 = [[NSMutableAttributedString alloc] initWithString:@" paid members can login to connect their account with the Voki mobile app.\nAll accounts that are"];
    UIFont *font2 = [UIFont fontWithName:@"Futura-Medium" size:18];
    [textBeforeIcon2 addAttribute:NSFontAttributeName value:font2 range:NSMakeRange(0, [textBeforeIcon2 length])];
    [completeText appendAttributedString:textBeforeIcon2];
    
    NSMutableAttributedString *textBeforeIcon3 = [[NSMutableAttributedString alloc] initWithString:@" Level 1"];
    [textBeforeIcon3 addAttribute:NSFontAttributeName value:font1 range:NSMakeRange(0, [textBeforeIcon3 length])];
    [completeText appendAttributedString:textBeforeIcon3];
    
    NSMutableAttributedString *textBeforeIcon4 = [[NSMutableAttributedString alloc] initWithString:@" and higher will be able to login, unlocking hundreds of "];
    [textBeforeIcon4 addAttribute:NSFontAttributeName value:font2 range:NSMakeRange(0, [textBeforeIcon4 length])];
    [completeText appendAttributedString:textBeforeIcon4];
    [completeText appendAttributedString:attachmentString];
    
    NSMutableAttributedString *textAfterIcon1 = [[NSMutableAttributedString alloc] initWithString:@"Premium Voki avatars. This includes all students connected to the account. You only need to login once.\n"];
    [textAfterIcon1 addAttribute:NSFontAttributeName value:font2 range:NSMakeRange(0, [textAfterIcon1 length])];
    [completeText appendAttributedString:textAfterIcon1];
    
    NSMutableAttributedString *textAfterIcon2 = [[NSMutableAttributedString alloc] initWithString:@"Please note that each account has a"];
    [textAfterIcon2 addAttribute:NSFontAttributeName value:font1 range:NSMakeRange(0, [textAfterIcon2 length])];
    [completeText appendAttributedString:textAfterIcon2];
    
    NSMutableAttributedString *textAfterIcon3 = [[NSMutableAttributedString alloc] initWithString:@" two device limit."];
    [textAfterIcon3 addAttribute:NSFontAttributeName value:font1 range:NSMakeRange(0, [textAfterIcon3 length])];
    [textAfterIcon3 addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:NSMakeRange(0, textAfterIcon3.length-1)];
    [completeText appendAttributedString:textAfterIcon3];
    
    [completeText addAttribute:NSForegroundColorAttributeName value:COLOR_BLUE range:NSMakeRange(0, completeText.length)];
    
    targetLabel.textAlignment=NSTextAlignmentLeft;
    targetLabel.attributedText=completeText;
    targetLabel.numberOfLines = 10;
}
-(void)AddBorderOnView:(UIView*)targetView withWidth:(CGFloat)borderwidth cornerRadius:(CGFloat)cornerRadius
{
    UIColor *COLOR_BLUE = [UIColor colorWithRed:31/255.0 green:47/255.0 blue:104/255.0 alpha:1.0];
    
    targetView.layer.borderColor = COLOR_BLUE.CGColor;
    targetView.layer.borderWidth = borderwidth;
    targetView.layer.cornerRadius = cornerRadius;
    targetView.layer.masksToBounds = YES;
}

-(UITableViewCell*)cellForAssignmentList:(UITableView*)tableView withIndex:(NSIndexPath*)indexPath
{
    UIColor* COLOR_BLUE = [UIColor colorWithRed:31/255.0 green:47/255.0 blue:104/255.0 alpha:1.0];
    
    AssignmetsListTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"assignmentlistcell" forIndexPath:indexPath];
    
    NSString *classid = [[_allClassesId objectAtIndex:indexPath.section] mutableCopy];
    NSArray *allLessons = [_allAssignments valueForKeyPath:[NSString stringWithFormat:@"%@.lesson",classid]];
    NSDictionary *lessonDetail = [allLessons objectAtIndex:indexPath.row];
    
    bool hasFinishDate = [lessonDetail objectForKey:@"has_finish_dt"];
    
    NSString *dateString;
    if(hasFinishDate == true)
    {
        dateString = [lessonDetail objectForKey:@"finish_dt"];
        if(![dateString isEqualToString:@"0000-00-00"])
        {
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            NSDate *date = [dateFormat dateFromString:dateString];
            [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
            [dateFormat setDateFormat:@"dd/MM/yyyy"];
            NSString *finalDate = [dateFormat stringFromDate:date];
            
            dateString = [NSString stringWithFormat:@" (Due %@)",finalDate];
        }
        else
        {
            dateString = @"";
        }
        
    }
    else
    {
        
    }
    
    NSMutableAttributedString *datestring1 = [[NSMutableAttributedString alloc] initWithString:[lessonDetail valueForKey:@"lessonName"]];
    [datestring1 addAttribute:NSForegroundColorAttributeName value:COLOR_BLUE range:NSMakeRange(0, datestring1.length)];
    UIFont *font1 = [UIFont fontWithName:@"Futura-Medium" size:21];
    [datestring1 addAttribute:NSFontAttributeName value:font1 range:NSMakeRange(0, [datestring1 length])];
    
    NSMutableAttributedString *datestring2 = [[NSMutableAttributedString alloc] initWithString:dateString];
    [datestring2 addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(0, datestring2.length)];
    UIFont *font2 = [UIFont fontWithName:@"Futura-Medium" size:12];
    [datestring2 addAttribute:NSFontAttributeName value:font2 range:NSMakeRange(0, [datestring2 length])];
    
    [datestring1 appendAttributedString:datestring2];
    
    cell.subTitleLabel.attributedText = datestring1;
    
    NSNumber *hasAssignment = [lessonDetail objectForKey:@"has_assignment"];
    
    if([hasAssignment integerValue] == 1)
    {
        cell.writingRequiredLabel.hidden = false;
    }
    else
    {
        cell.writingRequiredLabel.hidden = true;
    }
    
    NSArray *imagearr = [[NSArray alloc] initWithObjects:@"allAssignmentIcon",@"getStartedAssignment",@"inProgressAssignment",@"ReadyForReviewAssign",@"tryAgainAssignment",@"approvedAssignment", nil];
    NSArray *arr1 = [[NSArray alloc] initWithObjects:@"All Assignments",@"Get Started",@"In Progress",@"Ready For Review",@"Try Again",@"Approved", nil];
    NSUInteger indexOfStatus = [arr1 indexOfObject:[lessonDetail valueForKey:@"status"]];
    
    NSTextAttachment *imageAttachment = [[NSTextAttachment alloc] init];
    imageAttachment.image = [UIImage imageNamed:[imagearr objectAtIndex:indexOfStatus]];
    CGFloat imageOffsetY = -5.0;
    imageAttachment.bounds = CGRectMake(0, imageOffsetY, 18, 18);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:imageAttachment];
    NSMutableAttributedString *completeText= [[NSMutableAttributedString alloc] initWithString:@""];
    [completeText appendAttributedString:attachmentString];
    
    NSMutableAttributedString *textAfterIcon = [[NSMutableAttributedString alloc] initWithString:[lessonDetail valueForKey:@"status"]];
    [textAfterIcon addAttribute:NSForegroundColorAttributeName value:COLOR_BLUE range:NSMakeRange(0, textAfterIcon.length)];
    UIFont *font3 = [UIFont fontWithName:@"Futura-Medium" size:14];
    [textAfterIcon addAttribute:NSFontAttributeName value:font3 range:NSMakeRange(0, [textAfterIcon length])];
    [completeText appendAttributedString:textAfterIcon];
    
    cell.filterTypeLabel.attributedText = completeText;
    
    if([[lessonDetail valueForKey:@"lessonDesc"] isKindOfClass:[NSNull class]])
    {
        cell.descriptionLabel.text = @"";
    }
    else
    {
        cell.descriptionLabel.text = [lessonDetail valueForKey:@"lessonDesc"];
    }
    
    if([[lessonDetail valueForKey:@"feedback"] isKindOfClass:[NSNull class]])
    {
        cell.commentDescLabel.text = @"";
    }
    else
    {
        cell.commentDescLabel.text = [lessonDetail valueForKey:@"feedback"];
    }
    
    cell.attachmentButtn.tag = indexPath.row;
    
    if([[lessonDetail valueForKey:@"attachment_path"] isKindOfClass:[NSNull class]])
    {
        cell.attachmentButtn.hidden = true;
    }
    else
    {
        NSString *urlString = [lessonDetail valueForKey:@"attachment_path"];
        if(![urlString isEqualToString:@""])
        {
            cell.attachmentButtn.hidden = false;
        }
        else
        {
            cell.attachmentButtn.hidden = true;
        }
    }
    
    
    
    [self SetAllViews:cell tableView:tableView withIndex:indexPath];
    
    return cell;
}
-(CGFloat)heightForCellAssignmentList:(UITableView*)tableView withIndex:(NSIndexPath*)indexPath
{
    
    AssignmetsListTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"assignmentlistcell"];
    
    CGFloat heightForDescriptionLbl;
    CGFloat heightForCommentLbl;
    
    NSString *classid = [[_allClassesId objectAtIndex:indexPath.section] mutableCopy];
    NSArray *allLessons = [_allAssignments valueForKeyPath:[NSString stringWithFormat:@"%@.lesson",classid]];
    NSDictionary *lessonDetail = [allLessons objectAtIndex:indexPath.row];
    
    if([[lessonDetail valueForKey:@"lessonDesc"] isKindOfClass:[NSNull class]])
    {
        heightForDescriptionLbl = 20.0;
    }
    else
    {
        
       heightForDescriptionLbl = [self heightForLabel:[lessonDetail valueForKey:@"lessonDesc"] font:cell.descriptionLabel.font width:tableView.frame.size.width-34.0];
    }
    
    if([[lessonDetail valueForKey:@"feedback"] isKindOfClass:[NSNull class]])
    {
        heightForCommentLbl = 20.0;
    }
    else
    {
        heightForCommentLbl = [self heightForLabel:[lessonDetail valueForKey:@"feedback"] font:cell.commentDescLabel.font width:tableView.frame.size.width-34.0];
        
        if(heightForCommentLbl<20)
        {
            heightForCommentLbl = 20.0;
        }
    }
    
    CGFloat finalCellHeight = 31+2+24+2+(heightForDescriptionLbl+5+20+2+heightForCommentLbl+2)+5+1+10;
    
    return finalCellHeight;
}
-(void)SetAllViews:(AssignmetsListTableCell*)cell tableView:(UITableView*)tableView withIndex:(NSIndexPath*)indexPath
{
    CGFloat heightForDescriptionLbl;
    CGFloat heightForCommentLbl;
    
    NSString *classid = [[_allClassesId objectAtIndex:indexPath.section] mutableCopy];
    NSArray *allLessons = [_allAssignments valueForKeyPath:[NSString stringWithFormat:@"%@.lesson",classid]];
    NSDictionary *lessonDetail = [allLessons objectAtIndex:indexPath.row];
    
    if([[lessonDetail valueForKey:@"lessonDesc"] isKindOfClass:[NSNull class]])
    {
        heightForDescriptionLbl = 20.0;
    }
    else
    {
        
        heightForDescriptionLbl = [self heightForLabel:[lessonDetail valueForKey:@"lessonDesc"] font:cell.descriptionLabel.font width:tableView.frame.size.width-34.0];
    }
    
    if([[lessonDetail valueForKey:@"feedback"] isKindOfClass:[NSNull class]])
    {
        heightForCommentLbl = 20.0;
    }
    else
    {
        heightForCommentLbl = [self heightForLabel:[lessonDetail valueForKey:@"feedback"] font:cell.commentDescLabel.font width:tableView.frame.size.width-34.0];
        
        if(heightForCommentLbl<20)
        {
            heightForCommentLbl = 20.0;
        }
    }
    
    cell.view1.frame = CGRectMake(0, 0, cell.view1.frame.size.width, 31);
    cell.view2.frame = CGRectMake(0, 33, cell.view2.frame.size.width, 24);
    
    cell.view3.frame = CGRectMake(0, 59, cell.view3.frame.size.width, heightForDescriptionLbl+5+20+2+heightForCommentLbl+2);
    cell.descriptionLabel.frame = CGRectMake(17, 0, tableView.frame.size.width-34.0, heightForDescriptionLbl);
    cell.commentLabel.frame = CGRectMake(17, heightForDescriptionLbl+5, tableView.frame.size.width-34.0, 20);
    cell.commentDescLabel.frame = CGRectMake(17, heightForDescriptionLbl+5+20+2, cell.commentDescLabel.frame.size.width, heightForCommentLbl);
    
    UIFont *font1 = [UIFont fontWithName:@"Futura-Medium" size:15];
    int numberlines1 = (heightForDescriptionLbl/font1.lineHeight);
    cell.descriptionLabel.numberOfLines = numberlines1+1;
    
    UIFont *font2 = [UIFont fontWithName:@"Futura-MediumItalic" size:14];
    int numberlines2 = (heightForDescriptionLbl/font2.lineHeight);
    cell.commentDescLabel.numberOfLines = numberlines2+1;
    
    cell.separatorView.frame = CGRectMake(15, 59+heightForDescriptionLbl+27+heightForCommentLbl+10, cell.separatorView.frame.size.width, cell.separatorView.frame.size.height);
}

 -(CGFloat)heightForLabel:(NSString*)text font:(UIFont*)font width:(CGFloat)width
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, CGFLOAT_MAX)];
    
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.font = font;
    label.text = text;
    [label sizeToFit];
    
    return label.frame.size.height;
}
@end
