//
//  StringEncoder.h
//  VokiApp
//
//  Created by brst on 1/20/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringEncoder : NSObject

+ (StringEncoder *)sharedMethod;
- (NSString *)base64String:(NSString *)str;
- (NSString *) URLEncodedString_ch:(NSString*)String;

@end
