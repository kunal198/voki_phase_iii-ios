//
//  AssignmentListHeader.m
//  VokiApp
//
//  Created by brst on 19/01/18.
//  Copyright © 2018 brst. All rights reserved.
//

#import "AssignmentListHeader.h"

@implementation AssignmentListHeader

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _title_Label.minimumFontSize = 10.0;
    _title_Label.adjustsFontSizeToFitWidth = YES;
    _title_Label.numberOfLines = 1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
