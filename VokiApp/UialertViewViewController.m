//
//  UialertViewViewController.m
//  Pure
//
//  Created by netset on 10/6/15.
//  Copyright (c) 2015 Rahul Mehndiratta. All rights reserved.
//

#import "UialertViewViewController.h"

@interface UialertViewViewController ()
{
    NSRange range;
}
@end

@implementation UialertViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //    NSString *Restoretext = @"<font color=\"white\"><u>restore purchases</u></font>";
    //
    //    NSAttributedString *Attributted = [[NSAttributedString alloc] initWithData:[Restoretext dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    //    self.Alertrestorelabel.attributedText = Attributted;
    //    self.Alertrestorelabel.tintColor = [UIColor orangeColor];
    //
    //
    //    NSString *Learnmoretext = @"<font color=\"#fc7d00\"><span style=\"font-family: Arial; font-size: 18\"><u> Learn More</u></span></font>";
    //
    //    NSAttributedString *Attributtedlearnmore = [[NSAttributedString alloc] initWithData:[Learnmoretext dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    //    [self.unblocklearnmore setAttributedTitle:Attributtedlearnmore forState:UIControlStateNormal];
    //
    //    self.unblocklearnmore.tintColor = [UIColor orangeColor];
    //
    //    NSString *vokilogintext = @"<font color=\"#fc7d00\"><span style=\"font-family: Arial; font-size: 18\"><u>(Login) </u></span></font>";
    //    NSAttributedString *Attributtedlogin= [[NSAttributedString alloc] initWithData:[vokilogintext dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    //    [self.vokilogin setAttributedTitle:Attributtedlogin forState:UIControlStateNormal];
    //    self.vokilogin.tintColor = [UIColor blueColor];
    //
    //
    //    _buyallavatarstext.textAlignment = NSTextAlignmentJustified;
    //
    //    NSString *htmlStrin = @"<font color=\"white\"><span style=\"font-family: Arial ; font-size: 18\">Or unlock hundreds of <br> voki avatars.</font></span><font color=\"#fc7d00\"><span style=\"font-family: Arial; font-size: 18\"><u>Learn More</u></span>.</font>";
    //
    //    NSAttributedString *attributedStrin = [[NSAttributedString alloc] initWithData:[htmlStrin dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    //    _unblockcharORLabel.attributedText = attributedStrin;
    //
    //    NSString *htmlString = @"<font color=\"white\"><span style=\"font-family: Arial; font-size: 20\"><u>restore purchases</u></span>.</font>";
    //
    //    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    //    _Alertrestorelabel.attributedText = attributedString;
    
    _blackView.layer.cornerRadius=15;
    _blackView.clipsToBounds=YES;
    
    //[unblocklearnmorealert ];
    //    _whiteborderView.layer.cornerRadius=15;
    //    _whiteborderView.clipsToBounds=YES;
    //    _whiteborderView.layer.borderWidth = 2;
    //    _whiteborderView.layer.borderColor = [[UIColor whiteColor]CGColor];
    
    //    _ratebackview.layer.cornerRadius=15;
    //    _ratebackview.clipsToBounds=YES;
    //    _ratebackview.layer.borderWidth = 2;
    //    _ratebackview.layer.borderColor = [[UIColor whiteColor]CGColor];
    
    //_deleteView.layer.cornerRadius=15;
    //_deleteView.clipsToBounds=YES;
    //_deleteView.layer.borderWidth = 2;
    //_deleteView.layer.borderColor = [[UIColor whiteColor]CGColor];
    
    //    _ratealert.layer.cornerRadius=15;
    //    _ratealert.clipsToBounds=YES;
    //    _ratealert.layer.borderWidth = 2;
    //    _ratealert.layer.borderColor = [[UIColor whiteColor]CGColor];
    
    _unblockLearnmore.layer.cornerRadius=15;
    _unblockLearnmore.clipsToBounds=YES;
    _unblockLearnmore.layer.borderWidth = 2;
    _unblockLearnmore.layer.borderColor = [[UIColor whiteColor]CGColor];
    _unblockvookichar.layer.borderColor = [[UIColor whiteColor]CGColor];
    
    _thankyoualert.layer.cornerRadius=15;
    _thankyoualert.clipsToBounds=YES;
    _thankyoualert.layer.borderWidth = 2;
    _thankyoualert.layer.borderColor = [[UIColor whiteColor]CGColor];
    
    _nobtn.layer.cornerRadius=10;
    _nobtn.clipsToBounds=YES;
    //_nobtn.layer.borderWidth = 2;
    
    _yesBtn.layer.cornerRadius=10;
    _yesBtn.clipsToBounds=YES;
    //_yesBtn.layer.borderWidth = 2;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        _whiteborderView.layer.borderWidth = 5;
        _deleteView.layer.borderWidth = 5;
        _lblText.font = [UIFont systemFontOfSize:20];
        _deleteCharLbl.font = [UIFont systemFontOfSize:20];
    }
    
    CGFloat borderWidth = 3.0f;
    UIColor *borderColor = [UIColor colorWithRed:31/255.0 green:47/255.0 blue:104/255.0 alpha:1.0];
    _unblockVookiChar_BkgView.layer.borderColor = borderColor.CGColor;
    _unblockVookiChar_BkgView.layer.borderWidth = borderWidth;
    _unblockVookiChar_BkgView.layer.cornerRadius=10;
    _unblockVookiChar_BkgView.clipsToBounds=YES;
    _unblockvookichar.layer.cornerRadius=15;
    _unblockvookichar.clipsToBounds=YES;
    
    _unblockLearnmore_BkgView.layer.borderColor = borderColor.CGColor;
    _unblockLearnmore_BkgView.layer.borderWidth = borderWidth;
    _unblockLearnmore_BkgView.layer.cornerRadius=10;
    _unblockLearnmore_BkgView.clipsToBounds=YES;
    _unblockLearnmore.layer.cornerRadius=15;
    _unblockLearnmore.clipsToBounds=YES;
    
    _deleteView_BkgView.layer.borderColor = borderColor.CGColor;
    _deleteView_BkgView.layer.borderWidth = 4.5;
    _deleteView_BkgView.layer.cornerRadius=10;
    _deleteView_BkgView.clipsToBounds=YES;
    _deleteView.layer.cornerRadius=15;
    _deleteView.clipsToBounds=YES;
    
    _ratebackview.layer.borderColor = borderColor.CGColor;
    _ratebackview.layer.borderWidth = 4.5;
    _ratebackview.layer.cornerRadius=10;
    _ratebackview.clipsToBounds=YES;
    _ratealert.layer.cornerRadius=15;
    _ratealert.clipsToBounds=YES;
    
    _whiteBorderBkgView.layer.borderColor = borderColor.CGColor;
    _whiteBorderBkgView.layer.borderWidth = 4.5;
    _whiteBorderBkgView.layer.cornerRadius=10;
    _whiteBorderBkgView.clipsToBounds=YES;
    _whiteborderView.layer.cornerRadius=15;
    _whiteborderView.clipsToBounds=YES;
    
    NSLog(@"%f",_unblocklearnmorealert.font.pointSize);
    //_unblocklearnmorealert.font = [UIFont systemFontOfSize:_unblocklearnmorealert.font.pointSize];
    [self setFontsandViewSizes];
    [self textDidChange];
    
    // Do any additional setup after loading the view.
}
- (void) setFontsandViewSizes
{
    CGFloat _canvasWidth = self.view.frame.size.width;
    NSLog(@"%@",NSStringFromCGRect(_dollarUnlockIcon.frame));
    if(_canvasWidth == 812) {
        ///////// Unblock this Voki View
        [_orUnlockDescLabel setFont:[UIFont fontWithName:@"Futura-Medium" size:_orUnlockDescLabel.font.pointSize+2]];
        [_Alertrestorelabel setFont:[UIFont fontWithName:@"Futura-Medium" size:_Alertrestorelabel.font.pointSize+2]];
        _dollarUnlockIcon.frame = CGRectMake(_dollarUnlockIcon.frame.origin.x+18, _dollarUnlockIcon.frame.origin.y-3, _dollarUnlockIcon.frame.size.width, _dollarUnlockIcon.frame.size.height);
    } else if(_canvasWidth == 480) {
        
    } else if(_canvasWidth == 568) {
        
        NSLog(@"%@",NSStringFromCGSize(_dollarUnlockIcon.frame.size));
        _dollarUnlockIcon.frame = CGRectMake(_dollarUnlockIcon.frame.origin.x-3, _dollarUnlockIcon.frame.origin.y, _dollarUnlockIcon.frame.size.width, _dollarUnlockIcon.frame.size.height);
        
    } else if(_canvasWidth == 667) {
        [_orUnlockDescLabel setFont:[UIFont fontWithName:@"Futura-Medium" size:_orUnlockDescLabel.font.pointSize+2]];
        [_Alertrestorelabel setFont:[UIFont fontWithName:@"Futura-Medium" size:_Alertrestorelabel.font.pointSize+2]];
        _dollarUnlockIcon.frame = CGRectMake(_dollarUnlockIcon.frame.origin.x, _dollarUnlockIcon.frame.origin.y-2, _dollarUnlockIcon.frame.size.width, _dollarUnlockIcon.frame.size.height);
        _whyBuyAllDescLabel.frame = CGRectMake(_whyBuyAllDescLabel.frame.origin.x, _whyBuyAllDescLabel.frame.origin.y, _whyBuyAllDescLabel.frame.size.width, _whyBuyAllDescLabel.frame.size.height-10);
    } else if(_canvasWidth == 736) {
        [_orUnlockDescLabel setFont:[UIFont fontWithName:@"Futura-Medium" size:_orUnlockDescLabel.font.pointSize+2]];
        [_Alertrestorelabel setFont:[UIFont fontWithName:@"Futura-Medium" size:_Alertrestorelabel.font.pointSize+2]];
        
        _dollarUnlockIcon.frame = CGRectMake(80, 41, 25, 25);
    }
}
- (void)textDidChange
{
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        
        _whyBuyAllDescLabel.minimumFontSize = 10.0;
        _whyBuyAllDescLabel.adjustsFontSizeToFitWidth = YES;
        _whyBuyAllDescLabel.numberOfLines = 10;
        
        _deleteCharLbl.minimumFontSize = 10.0;
        _deleteCharLbl.adjustsFontSizeToFitWidth = YES;
        _deleteCharLbl.numberOfLines = 3;
    });
}
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    for(NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
    
}



- (IBAction)closeBtn:(id)sender
{
    
}

-(UIView *)showPopUp
{
    return self.view;
}

- (IBAction)LearnMore:(id)sender {
}


@end
