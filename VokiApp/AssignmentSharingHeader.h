//
//  AssignmentSharingHeader.h
//  VokiApp
//
//  Created by brst on 24/01/18.
//  Copyright © 2018 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssignmentSharingHeader : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *title_label;
@property (strong, nonatomic) IBOutlet UIButton *dropDownButton;

@end
