//
//  AppDelegate.h
//  VokiApp
//
//  Created by brst on 12/8/15.
//  Copyright © 2015 brst. All rights reserved.
//

//com.marcel.slipperySlick
//com.brstDeveloper.SlipperySlick

#import <UIKit/UIKit.h>
#import <GooglePlus/GooglePlus.h>
#import "Reachability.h"
#import <StoreKit/StoreKit.h>

@class GTMOAuth2Authentication;

@interface AppDelegate : UIResponder <UIApplicationDelegate,SKProductsRequestDelegate,SKPaymentTransactionObserver,SKRequestDelegate,SKStoreProductViewControllerDelegate>

{
    UIActivityIndicatorView *activityView;
}
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,retain) NSString *google_str;
@property (nonatomic,retain) NSString *google_url;
@property (assign)BOOL isFacebookCalled;
@property (assign)BOOL isTwitterCalled;

@property (assign)BOOL isRecording;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

-(BOOL)ReturnConnection;
-(BOOL)internetActive;

@end
