//
//  AssignmentSharingTableCell.h
//  VokiApp
//
//  Created by brst on 24/01/18.
//  Copyright © 2018 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssignmentSharingTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *background_View;
@property (strong, nonatomic) IBOutlet UILabel *title_Label;
@property (strong, nonatomic) IBOutlet UILabel *status_Label;

@end
