//
//  DemoViewController.h
//  VokiApp
//
//  Created by brst on 17/01/18.
//  Copyright © 2018 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DemoViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *view1;
@property (strong, nonatomic) IBOutlet UIView *view2;
@property (strong, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UIButton *button1;
@property (strong, nonatomic) IBOutlet UIButton *continue1;

@property (strong, nonatomic) IBOutlet UIView *view3;
@property (strong, nonatomic) IBOutlet UIView *view4;
@property (strong, nonatomic) IBOutlet UILabel *cinfirmsubmition1;
@property (strong, nonatomic) IBOutlet UILabel *areyou1;
@property (strong, nonatomic) IBOutlet UIButton *back1;
@property (strong, nonatomic) IBOutlet UIButton *submit1;

@property (strong, nonatomic) IBOutlet UIView *view5;
@property (strong, nonatomic) IBOutlet UIView *view6;
@property (strong, nonatomic) IBOutlet UILabel *yourassignment1;

@property (strong, nonatomic) IBOutlet UIView *view7;
@property (strong, nonatomic) IBOutlet UIView *view8;
@property (strong, nonatomic) IBOutlet UIButton *continue2;
@property (strong, nonatomic) IBOutlet UIButton *info1;
@property (strong, nonatomic) IBOutlet UITableView *tableView1;

@property (strong, nonatomic) IBOutlet UIView *view9;
@property (strong, nonatomic) IBOutlet UIView *view10;
@property (strong, nonatomic) IBOutlet UILabel *properuselabel;

@property (strong, nonatomic) IBOutlet UIView *view11;
@property (strong, nonatomic) IBOutlet UIView *view12;
@property (strong, nonatomic) IBOutlet UILabel *properuselabel2;
@property (strong, nonatomic) IBOutlet UILabel *info2;
@property (strong, nonatomic) IBOutlet UILabel *thisassignmentlabel;
@property (strong, nonatomic) IBOutlet UITextView *textview1;
@property (strong, nonatomic) IBOutlet UIButton *continue3;
@property (strong, nonatomic) IBOutlet UIButton *back2;


@end
