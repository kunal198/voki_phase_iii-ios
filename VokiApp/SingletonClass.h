//
//  SingletonClass.h
//  VokiApp
//
//  Created by brst on 01/01/18.
//  Copyright © 2018 brst. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SingletonClass : NSObject

+ (SingletonClass *)sharedCenter;

-(BOOL)isWhatsappInstalled;
-(BOOL)isInstagramAppInstalled;
-(void)setWhyLoginAttributedDesc:(UILabel*)targetLabel;
-(void)AddBorderOnView:(UIView*)targetView withWidth:(CGFloat)borderwidth cornerRadius:(CGFloat)cornerRadius;
-(UITableViewCell*)cellForAssignmentList:(UITableView*)targetView withIndex:(NSIndexPath*)indexPath;
-(CGFloat)heightForCellAssignmentList:(UITableView*)tableView withIndex:(NSIndexPath*)indexPath;
-(CGFloat)heightForLabel:(NSString*)text font:(UIFont*)font width:(CGFloat)width;

@property (nonatomic, retain) NSMutableDictionary * allAssignments;
@property (nonatomic, retain) NSMutableArray * allClassesId;
@end
