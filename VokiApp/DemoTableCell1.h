//
//  DemoTableCell1.h
//  VokiApp
//
//  Created by brst on 18/01/18.
//  Copyright © 2018 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DemoTableCell1 : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *background_View;
@property (strong, nonatomic) IBOutlet UILabel *title_label;
@property (strong, nonatomic) IBOutlet UILabel *label2;

@end
