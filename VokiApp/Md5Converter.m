//
//  Md5Converter.m
//  VokiApp
//
//  Created by brst on 1/20/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "Md5Converter.h"
#import <CommonCrypto/CommonDigest.h>
#import <Foundation/Foundation.h>

@implementation Md5Converter

+ (Md5Converter *)sharedMethod
{
    static Md5Converter *sharedInstance;
    if (!sharedInstance)
    {
        sharedInstance = [[super alloc]init];
    }
    return sharedInstance;
}

- (NSString *)MD5String :(NSString *) input
{
    const char *cstr = [input UTF8String];
    unsigned char result[16];
    CC_MD5(cstr, strlen(cstr), result);
    
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];  
}

@end
