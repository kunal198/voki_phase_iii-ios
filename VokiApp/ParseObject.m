//
//  ParseObject.m
//  VokiApp
//
//  Created by brst on 1/21/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "ParseObject.h"

@implementation ParseObject

+ (ParseObject *)sharedMethod
{
    static ParseObject *sharedInstance;
    if (!sharedInstance)
    {
        sharedInstance = [[super alloc]init];
    }
    return sharedInstance;
}

#pragma mark - method to call parser

-(NSMutableArray*)ParseFile
{
    //-------- initializing objects ---------//
    _dataArray = [[[NSMutableArray alloc]init] autorelease];
    _typeArray = [[[NSMutableArray alloc]init] autorelease];
    _AccIDArray = [[[NSMutableArray alloc]init] autorelease];
    
    [[NSURLCache sharedURLCache] setMemoryCapacity:0];
    [[NSURLCache sharedURLCache] setDiskCapacity:0];
    
    //-------- getting local xml file path ---------//
    NSString *xmlPath = [[NSBundle mainBundle] pathForResource:@"Accessory" ofType:@"xml"];
    NSData *xmlData = [NSData dataWithContentsOfFile:xmlPath];
    
    //-------- creating parse objecting and calling its methods ---------//
    xmlParser = [[[NSXMLParser alloc] initWithData:xmlData] autorelease];
    xmlParser.delegate=self;
    [xmlParser parse];
    
    NSLog(@"Parsing Done");
    
    [xmlParser setDelegate:nil];
    xmlParser = nil;
//    [xmlParser release];
    
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"IS_XML_PARSED"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return _dataArray;
}

-(NSMutableArray*)CallDescXML:(int)IndexNo :(NSString*)XML_Path
{
    _dataArray = [[[NSMutableArray alloc]init] autorelease];
    _typeArray = [[[NSMutableArray alloc]init] autorelease];
    _AccIDArray = [[[NSMutableArray alloc]init] autorelease];
    
    [[NSURLCache sharedURLCache] setMemoryCapacity:0];
    [[NSURLCache sharedURLCache] setDiskCapacity:0];
    
    //-------- getting local xml file path ---------//
    NSString *xmlPath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@/desc",XML_Path] ofType:@"xml"];
    NSData *xmlData = [NSData dataWithContentsOfFile:xmlPath];
    
    //-------- creating parse objecting and calling its methods ---------//
    xmlParser = [[[NSXMLParser alloc] initWithData:xmlData] autorelease];
    xmlParser.delegate=self;
    [xmlParser parse];
    
    NSLog(@"Parsing Done.....");
    
    [xmlParser setDelegate:nil];
    xmlParser = nil;
//    [xmlParser release];
    
    return _dataArray;
}

#pragma mark - Parser Delegate Method

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if([elementName isEqualToString:@"Host"])
    {
        LastElement = [NSString stringWithFormat:@"%@",elementName] ;
        
        _hostColorString = [attributeDict objectForKey:@"colorString"];
        _hostSpritSheet = [attributeDict objectForKey:@"spriteSheet"];
        _hostAccString = [attributeDict objectForKey:@"accString"];
        genderID = [attributeDict objectForKey:@"gender"];
    }
    else if([elementName isEqualToString:@"Position"])
    {
        LastElement = [NSString stringWithFormat:@"%@",elementName] ;
        
        SCALE = [attributeDict objectForKey:@"SCALE"];
        YPOS = [attributeDict objectForKey:@"YPOS"];
        XPOS = [attributeDict objectForKey:@"XPOS"];
    }

    else if([elementName isEqualToString:@"Accessories"])
    {
        LastElement = [NSString stringWithFormat:@"%@",elementName] ;
        _typeArray = [[[NSMutableArray alloc]init] autorelease];
    }
    else if([elementName isEqualToString:@"Type"])
    {
        if(![LastElement isEqualToString:@"Types"])
        {
            int count = 0;
            
            for (NSString *key in attributeDict)
            {
                if(count == 0)
                {
                    count = 1;
                    _accIDString = [attributeDict objectForKey:key];
                }
                else
                {
                    [_typeArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:_accIDString,[attributeDict objectForKey:key], nil]];
                }
            }
        }
    }
    else if([elementName isEqualToString:@"Types"])
    {
        LastElement = [NSString stringWithFormat:@"%@",elementName] ;
        _AccIDArray = [[[NSMutableArray alloc]init] autorelease];
    }
    if([LastElement isEqualToString:@"Types"])
    {
        if([elementName isEqualToString:@"Type"])
        {
            _accTypeString = [attributeDict objectForKey:@"id"];
            _accString = [[NSString new] autorelease];
        }
        else if([elementName isEqualToString:@"Acc"])
        {
            if(_accString.length == 0)
            {
                _accString = [attributeDict objectForKey:@"id"];
            }
            else
            {
                _accString = [NSString stringWithFormat:@"%@,%@",_accString,[attributeDict objectForKey:@"id"]];
            }
        }
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if([elementName isEqualToString:@"Default"])
    {
    }
    if([LastElement isEqualToString:@"Types"])
    {
        if([elementName isEqualToString:@"Type"])
        {
            [_AccIDArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:_accString,_accTypeString, nil]];
        }
    }
//    if([elementName isEqualToString:@"Host"])
//    {
//        [_dataArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:
//                               _hostColorString, @"colorString",
//                               _hostSpritSheet, @"spriteSheet",
//                               _hostAccString, @"accString",
//                               _typeArray,@"Type",
//                               _AccIDArray,@"Types",
//                               nil]];
//        [xmlParser abortParsing];
//    }
    if([elementName isEqualToString:@"Host"])
    {
        [_dataArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                               _hostColorString, @"colorString",
                               _hostSpritSheet, @"spriteSheet",
                               _hostAccString, @"accString",
                               XPOS, @"XPOS",
                               YPOS, @"YPOS",
                               SCALE, @"SCALE",
                               genderID,@"genderID",
                               _typeArray,@"Type",
                               _AccIDArray,@"Types",
                               nil]];
    }
}

@end
