//
//  UIColor+GetHSB.m
//  ILColorPickerExample
//
//  Created by Jon Gilkison on 9/2/11.
//  Copyright 2011 Interfacelab LLC. All rights reserved.
//

#import "UIColor+GetHSB.h"


@implementation UIColor(GetHSB)

-(HSBType)HSB
{
    HSBType hsb;
    
    
    hsb.hue=0;
    hsb.saturation=0;
    hsb.brightness=0;
    
    CGColorSpaceModel model=CGColorSpaceGetModel(CGColorGetColorSpace([self CGColor]));
    
    if ((model==kCGColorSpaceModelMonochrome) || (model==kCGColorSpaceModelRGB))
    {
        const CGFloat *c = CGColorGetComponents([self CGColor]);  
        
        float x = fminf(c[0], c[1]);
        x = fminf(x, c[2]);
        
        float b = fmaxf(c[0], c[1]);
        b = fmaxf(b, c[2]);
        
        if (b == x) 
        {
            float hueO = [[NSUserDefaults standardUserDefaults]floatForKey:@"hue"];
            hsb.hue=hueO;
            hsb.saturation=0;
            hsb.brightness=b;
        }
        else
        {
             float canvasWidth = [[UIScreen mainScreen]bounds].size.width;
            
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
            {
                float f = (c[0] == x) ? c[1] - c[2] : ((c[1] == x) ? c[2] - c[0] : c[0] - c[1]);
                int i = (c[0] == x) ? 3 : ((c[1] == x) ? 5 : 1);
                
                hsb.hue=((i - f /(b - x))/6);
                hsb.saturation=(b - x)/b;
                hsb.brightness=b;
                [[NSUserDefaults standardUserDefaults]setFloat:hsb.hue forKey:@"hue"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            else
            {
                if (canvasWidth == 480)
                {
                    float f = (c[0] == x) ? c[1] - c[2] : ((c[1] == x) ? c[2] - c[0] : c[0] - c[1]);
                    int i = (c[0] == x) ? 3 : ((c[1] == x) ? 5 : 1);
                    
                    hsb.hue=((i - f /(b - x))/6);
                    hsb.saturation=(b - x)/b;
                    hsb.brightness=b;
                    [[NSUserDefaults standardUserDefaults]setFloat:hsb.hue forKey:@"hue"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                }
                
                else if (canvasWidth == 736)
                {
                    float f;
                    int i;
                    
                    NSString *red = [NSString stringWithFormat:@"%.5f",c[0]];
                    NSString *green = [NSString stringWithFormat:@"%.5f",c[1]];
                    
                    NSString *x1 = [NSString stringWithFormat:@"%.5f",x];
                    
                    if (red == x1)
                    {
                        f = (c[1] - c[2]);
                        i = 3;
                    }
                    
                    else if (green == x1) {
                        f = (c[2] - c[0]);
                        i=5;
                    }
                    
                    else
                    {
                        f = (c[0] - c[1]);
                        i=1;
                    }
                    
                    hsb.hue=((i - f /(b - x))/6);
                    [[NSUserDefaults standardUserDefaults]setFloat:hsb.hue forKey:@"hue"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    hsb.saturation=(b - x)/b;
                    hsb.brightness=b;

                }
                else if(canvasWidth == 667)
                {
                    float f;
                    int i;
                    
                    NSString *red = [NSString stringWithFormat:@"%.5f",c[0]];
                    NSString *green = [NSString stringWithFormat:@"%.5f",c[1]];
                    
                    NSString *x1 = [NSString stringWithFormat:@"%.5f",x];
                    
                    if (red == x1)
                    {
                        f = (c[1] - c[2]);
                        i = 3;
                    }
                    
                    else if (green == x1) {
                        f = (c[2] - c[0]);
                        i=5;
                    }
                    
                    else
                    {
                        f = (c[0] - c[1]);
                        i=1;
                    }
                    
                    hsb.hue=((i - f /(b - x))/6);
                    [[NSUserDefaults standardUserDefaults]setFloat:hsb.hue forKey:@"hue"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    hsb.saturation=(b - x)/b;
                    hsb.brightness=b;

                }
                else
                {
                    NSString *DEVICE = [[NSUserDefaults standardUserDefaults]objectForKey:@"DEVICE"];
                    
                    if ([DEVICE isEqualToString:@"iPhone 5 (GSM)"]||[DEVICE isEqualToString:@"iPhone 5 (GSM+CDMA)"])
                    {
                        float f = (c[0] == x) ? c[1] - c[2] : ((c[1] == x) ? c[2] - c[0] : c[0] - c[1]);
                        int i = (c[0] == x) ? 3 : ((c[1] == x) ? 5 : 1);
                        
                        hsb.hue=((i - f /(b - x))/6);
                        hsb.saturation=(b - x)/b;
                        hsb.brightness=b;
                        [[NSUserDefaults standardUserDefaults]setFloat:hsb.hue forKey:@"hue"];
                        [[NSUserDefaults standardUserDefaults]synchronize];

                    }
                    else
                    {
                    float f;
                    int i;
                    
                    NSString *red = [NSString stringWithFormat:@"%.5f",c[0]];
                    NSString *green = [NSString stringWithFormat:@"%.5f",c[1]];
                    
                    NSString *x1 = [NSString stringWithFormat:@"%.5f",x];
                    
                    if (red == x1)
                    {
                        f = (c[1] - c[2]);
                        i = 3;
                    }
                    
                    else if (green == x1) {
                        f = (c[2] - c[0]);
                        i=5;
                    }
                    
                    else
                    {
                        f = (c[0] - c[1]);
                        i=1;
                    }
                    
                    float hueO;
                     hueO = ((i - f /(b - x))/6);
                     hsb.hue=hueO;
                    [[NSUserDefaults standardUserDefaults]setFloat:hsb.hue forKey:@"hue"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    hsb.saturation=(b - x)/b;
                    hsb.brightness=b;

                }
                }
            }
           
            
            
    }
    }
    
    return hsb;
}


@end
