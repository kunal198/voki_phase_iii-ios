//
//  AssignmentListHeader.h
//  VokiApp
//
//  Created by brst on 19/01/18.
//  Copyright © 2018 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssignmentListHeader : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *title_Label;
@end
