//
//  UialertViewViewController.h
//  Pure
//
//  Created by netset on 10/6/15.
//  Copyright (c) 2015 Rahul Mehndiratta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UialertViewViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *lblText;
@property (strong, nonatomic) IBOutlet UIView *whiteborderView;
@property (strong, nonatomic) IBOutlet UIView *whiteBorderBkgView;
@property (strong, nonatomic) IBOutlet UIView *blackView;
@property (strong, nonatomic) IBOutlet UIButton *closeBtn;
- (IBAction)closeBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *deleteView;
@property (strong, nonatomic) IBOutlet UIView *deleteView_BkgView;
@property (strong, nonatomic) IBOutlet UIButton *nobtn;
@property (strong, nonatomic) IBOutlet UIButton *yesBtn;
@property (strong, nonatomic) IBOutlet UIButton *deleteCloseBtn;

@property (weak, nonatomic) IBOutlet UILabel *deleteCharLbl;
@property (strong, nonatomic) IBOutlet UIButton *learnmoreloginbtn;

@property (strong, nonatomic) IBOutlet UIView *ratebackview;

@property (strong, nonatomic) IBOutlet UIView *unblockvookichar;
@property (strong, nonatomic) IBOutlet UIView *unblockVookiChar_BkgView;

-(UIView *)showPopUp;
@property (strong, nonatomic) IBOutlet UIButton *vokiblockclose;
//@property (strong, nonatomic) IBOutlet UIButton *restorebtn;

@property (strong, nonatomic) IBOutlet UIButton *BuyProduct;

@property (strong, nonatomic) IBOutlet UIView *thankyoualert;

@property (strong, nonatomic) IBOutlet UIButton *closethankyoualert;

@property (strong, nonatomic) IBOutlet UIView *blackview1;

@property (strong, nonatomic) IBOutlet UILabel *Alertrestorelabel;

@property (strong, nonatomic) IBOutlet UIButton *unblocklearnmore;

@property (strong, nonatomic) IBOutlet UIView *unblockLearnmore;
@property (strong, nonatomic) IBOutlet UIView *unblockLearnmore_BkgView;

@property (strong, nonatomic) IBOutlet UILabel *buyallavatarstext;

@property (strong, nonatomic) IBOutlet UIButton *vokilogin;

@property (strong, nonatomic) IBOutlet UIButton *closeunblocklearnmore;
@property (strong, nonatomic) IBOutlet UILabel *orUnlockDescLabel;
@property (strong, nonatomic) IBOutlet UIImageView *dollarUnlockIcon;


@property (strong, nonatomic) IBOutlet UITextView *unblocklearnmorealert;
@property (strong, nonatomic) IBOutlet UILabel *whyBuyAllDescLabel;


@property (strong, nonatomic) IBOutlet UIButton *Learnmorebuyallbtn;

@property (strong, nonatomic) IBOutlet UIButton *unlockbuyallbtn;


@property (strong, nonatomic) IBOutlet UIButton *unblockrestoreAction;

@property (strong, nonatomic) IBOutlet UIView *ratealert;


@property (strong, nonatomic) IBOutlet UIButton *reviewbtn;

@property (strong, nonatomic) IBOutlet UIButton *remindlaterbtn;

@property (strong, nonatomic) IBOutlet UIButton *dontaskagainbtn;
@property (strong, nonatomic) IBOutlet UILabel *unlockVokiBtn;

@end
