//
//  SocialShare.swift
//  Ludi
//
//  Created by Mrinal Khullar on 07/08/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

enum SharingPlatform {
    case Facebook
    case Twitter
    case Text
    case Email
    case Default
}

@objc public class SocialShare: NSObject,TWTRComposerViewControllerDelegate,TWTRTweetViewDelegate {
    
    var shareItem = String()
    var campaignId = String()
    var submissionId = String()
    var dictionary = NSMutableDictionary()
    
    class var swiftSharedInstance: SocialShare {
        struct Singleton {
            static let instance = SocialShare()
        }
        return Singleton.instance
    }
    
    // the sharedInstance class method can be reached from ObjC
     func sharedInstance() -> SocialShare {
        return SocialShare.swiftSharedInstance
    }
    
    // Some testing
    func testTheSingleton() -> String {
        return "Hello World"
    }
    
 
    
    // MARK: Twitter Share
    
    func shareImageWithTwitter(image: UIImage, text: String, viewController : UIViewController, completion: @escaping (Bool) -> (), Error: @escaping (NSError) -> ())  {
        
        if (Twitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
            
            let composer = TWTRComposer()
            var txtString : String
            if self.dictionary.value(forKey: "isCampaign") as! Bool{
                txtString = "Ludus by \(self.dictionary.value(forKey: "username") as! String) on Ludi. Download the Ludi app: www.ludiapp.com"
            }
            else{
                txtString = "Post by \(self.dictionary.value(forKey: "username") as! String) on Ludi. Download the Ludi app: www.ludiapp.com"
            }
            
            composer.setText(txtString)
            composer.setImage(image)
            composer.show(from: viewController) { (result) in
                self.twitterComposerResult(result: result)
                completion(true)
            }
        } else {
            
            Twitter.sharedInstance().logIn { session, error in
                if(error != nil){
                    Error(error! as NSError)
                }
                
                if session != nil {
                    
                    let composer = TWTRComposer()
                    var txtString : String
                    if self.dictionary.value(forKey: "isCampaign") as! Bool{
                        txtString = "Ludus by \(self.dictionary.value(forKey: "username") as! String) on Ludi. Download the Ludi app: www.ludiapp.com"
                    }
                    else{
                        txtString = "Post by \(self.dictionary.value(forKey: "username") as! String) on Ludi. Download the Ludi app: www.ludiapp.com"
                    }
                    
                    composer.setText(txtString)
                    composer.setImage(image)
                    composer.show(from: viewController) { (result) in
                        self.twitterComposerResult(result: result)
                        completion(true)
                    }
                }
                else {
                    completion(false)
                    
                }
            }
        }
        
    }
    
    func shareLinkWithTwitter(link: URL, text: String, viewController : UIViewController, completion: @escaping (Bool) -> (), Error: @escaping (NSError) -> ())  {
        
        if (Twitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
            
            let composer = TWTRComposer()
            composer.setText(text)
            composer.setURL(link)
            composer.show(from: viewController) { (result) in
                self.twitterComposerResult(result: result)
                completion(true)
            }
        }
        else{
            Twitter.sharedInstance().logIn { session, error in
                if(error != nil){
                    Error(error! as NSError)
                }
                
                if session != nil {
                    
                    let composer = TWTRComposer()
                    composer.setText(text)
                    composer.setURL(link)
                    composer.show(from: viewController) { (result) in
                        self.twitterComposerResult(result: result)
                        completion(true)
                    }
                }
                else {
                    completion(false)
                    
                }
            }
        }
        
    }
    
    
   public func shareVideoWithTwitter(initialText: String,fileURL: URL, viewController : UIViewController, completion: @escaping (Bool) -> (), Error: @escaping (NSError) -> ())  {
        
        if (Twitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
            
            let txtString = "Voki"

            
            print(fileURL.scheme)
            
            let composer = TWTRComposerViewController(initialText:txtString, image: nil, videoURL:fileURL)
            composer.delegate = self
            viewController.present(composer, animated: true, completion: {
                completion(true)
            })
            
        } else {
            
            Twitter.sharedInstance().logIn { session, error in
                if(error != nil){
                    print(error?.localizedDescription)
                    Error(error! as NSError)
                }
                if session != nil {
                    
                    let txtString = "Voki"
                    
                    let composer = TWTRComposerViewController(initialText: txtString, image: nil, videoURL:fileURL)
                    composer.delegate = self
                    viewController.present(composer, animated: true, completion: {
                        completion(true)
                    })
                    
                }
                else {
                    completion(false)
                }
            }
        }
        
    }
    

    //MARK: Twitter Delegates
    
    public func composerDidCancel(_ controller: TWTRComposerViewController){
        
    }
    public func composerDidFail(_ controller: TWTRComposerViewController, withError error: Error) {
        
    }
    public func composerDidSucceed(_ controller: TWTRComposerViewController, with tweet: TWTRTweet) {
        
    }
    
    
    public func twitterComposerResult(result : TWTRComposerResult) {
        
        if(result == .done){
            
        }
        
    }
    
    

    
    
}
