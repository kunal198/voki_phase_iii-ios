//
//  AssignmentSharingHeader.m
//  VokiApp
//
//  Created by brst on 24/01/18.
//  Copyright © 2018 brst. All rights reserved.
//

#import "AssignmentSharingHeader.h"

@implementation AssignmentSharingHeader

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _title_label.minimumFontSize = 10.0;
    _title_label.adjustsFontSizeToFitWidth = YES;
    _title_label.numberOfLines = 1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
