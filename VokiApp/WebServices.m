//
//  WebServices.m
//  VokiApp
//
//  Created by brst on 19/01/18.
//  Copyright © 2018 brst. All rights reserved.
//

#import "WebServices.h"

@implementation WebServices

static WebServices *sharedAwardCenter = nil;

+ (WebServices *)sharedCenter {
    if (sharedAwardCenter == nil) {
        sharedAwardCenter = [[super allocWithZone:NULL] init];
    }
    return sharedAwardCenter;
}

- (id)init {
    if ( (self = [super init]) ) {
        // your custom initialization
    }
    return self;
}

@end
